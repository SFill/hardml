from flask import Flask
from flask_redis import Redis as init_redis
from redis import Redis
import os
import requests

app = Flask(__name__)

app.config["REDIS_HOST"] = "65.21.177.167"
app.config["REDIS_PORT"] = 6379
app.config["REDIS_DB"] = 0
app.config["REDIS_PASSWORD"] = "nelson"

redis: Redis = init_redis(app).connection

# redis: Redis = Redis()

replica_name = f"replica_{os.environ['APP_HOST']}:{os.environ['APP_PORT']}"

_SECRET_NUMBER = 0
import json

def register_server():
    print("start registering service")
    global _SECRET_NUMBER
    # while True:
    #     resp = requests.get("https://lab.karpov.courses/hardml-api/module-5/get_secret_number")
        
    #     if resp.status_code != 200:
    #         print(f"not ok:{resp.status_code},{resp.text}")
    #         continue
    #     break
    # resp_data = resp.json()
    with open("secret_number.json") as f:
        _json = json.load(f)
    _SECRET_NUMBER = _json['secret_number']

    # redis.hset(
    #     replica_name,
    #     mapping={
    #         "host": os.environ["APP_HOST"],
    #         "port": os.environ["APP_PORT"],
    #     },
    # )
    # redis.lpush("web_app", replica_name)


def unregister_server(signal_number, frame):
    print("start unregistering service")
    # redis.delete(replica_name)
    # redis.lrem("web_app", 0, value=replica_name)
    # print(f"Received: {signal_number}")
    exit(signal_number)


import signal

signal.signal(signal.SIGTERM, unregister_server)
signal.signal(signal.SIGINT, unregister_server)
# signal.signal(signal.SIGKILL, unregister_server)

register_server()
import time

@app.route("/return_secret_number")
def index():
    time.sleep(1)
    return {'secret_number': _SECRET_NUMBER}
