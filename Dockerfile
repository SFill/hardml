
FROM python:3.9.12

RUN python -m pip install Flask==2.2.2 Flask-And-Redis requests

WORKDIR app

# COPY main.py main.py

COPY . .

ENV APP_PORT 80
ENV APP_HOST localhost

# RUN python -m pip install gunicorn 
RUN python -m pip install pyuwsgi gevent


ENTRYPOINT uwsgi --master \
  --ini /app/config.ini \
  --single-interpreter \
  --workers 2 \
  --gevent 10 \
  --http 0.0.0.0:${APP_PORT} \
  --module wsgi:gunicorn_app